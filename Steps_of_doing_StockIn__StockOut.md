# Steps of doing StockIn/ StockOut

### Stock In
1. Log In to Superspares Admin System http://iisawms.super10.com.au/
2. Select "StockItemHolding" from the left panel.
3. Select "StockIn" from the top
4. After that, a window to input stock barcode and stock quantity will appear. In that window, select "Scan barcode" field and scan the barcode of the item to be added using barcode scanner
5. After the item has been scanned, if the item is an existing item in "StockItemHolding" list, a blue box showing existing stock location and container to put the item in will show up. 
6. Enter the quantity of the item that is going to be added.
7. Click Save

### Stock Out


1. Log In to Superspares Admin System http://iisawms.super10.com.au/
2. Select "StockItemHolding" from the left panel.
3. Search for the item using the search function
4. When the chosen item appears, check if the item is locked or not (Normally 0 = not locked, > 0 = locked)
NOTE: Only do StockOut when item is locked (Ordering employees are the ones who choose the to lock an item)
5. Click StockOut
6. Entet the quantity of stockout in "StockoutQuantity"
7. Click "Save Changes" when done.