# Steps of doing StockIn/ StockOut

### Stock In
1. Log In to Superspares Admin System http://iisawms.super10.com.au/
2. Select "StockItemHolding" from the left panel.
![step2](http://sp.super10.com.au/sites/wiki/Screenshots/1a.jpg)
3. Select "StockIn" from the top
![step3](http://sp.super10.com.au/sites/wiki/Screenshots/2a.jpg?csf=1&e=am4FUo)
4. After that, a window to input stock barcode and stock quantity will appear. In that window, select "Scan barcode" field and scan the barcode of the item to be added using barcode scanner
![step4](http://sp.super10.com.au/sites/wiki/Screenshots/3a.jpg?csf=1&e=50JohI)
5. After the item has been scanned, if the item is an existing item in "StockItemHolding" list, a blue box showing existing stock location and container to put the item in will show up. 
![step5-1](http://sp.super10.com.au/sites/wiki/Screenshots/4a.jpg?csf=1&e=8JB1mt)

![step5-2](http://sp.super10.com.au/sites/wiki/Screenshots/5a.jpg?csf=1&e=8Kxor1)

From the example, the location to add the stock is 256-L5-B9 and the warehouse is currently having 8 of selected item.

6. Enter the quantity of the item that is going to be added.

![step6](http://sp.super10.com.au/sites/wiki/Screenshots/6a.jpg?csf=1&e=kvvHmP)

7. Click Save
![step7](http://sp.super10.com.au/sites/wiki/Screenshots/7a.jpg?csf=1&e=fmmUbI)

### Stock Out


1. Log In to Superspares Admin System http://iisawms.super10.com.au/
2. Select "StockItemHolding" from the left panel.
![step2](http://sp.super10.com.au/sites/wiki/Screenshots/1a.jpg?csf=1&e=Kn2i1W)

3. Search for the item using the search function
![enter image description here](http://sp.super10.com.au/sites/wiki/Screenshots/so1.JPG?csf=1&e=Fccu7S)
4. When the chosen item appears, check if the item is locked or not (Normally 0 = not locked, > 0 = locked)
![step4](http://sp.super10.com.au/sites/wiki/Screenshots/so3.JPG?csf=1&e=aGTYe4)
NOTE: Only do StockOut when item is locked (Ordering employees are the ones who choose the to lock an item)
5. Click StockOut
![step5](http://sp.super10.com.au/sites/wiki/Screenshots/so4.JPG?csf=1&e=EIb1PJ)
6. Enter the quantity of stockout in "StockoutQuantity"
![step6](http://sp.super10.com.au/sites/wiki/Screenshots/so6.JPG?csf=1&e=U0RCqY)
7. Click "Save Changes" when done.
![step7](http://sp.super10.com.au/sites/wiki/Screenshots/so7.JPG?csf=1&e=j0lvnj)
